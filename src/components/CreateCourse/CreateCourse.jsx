import { useState, useCallback, useMemo } from 'react';

import { v4 as uuidv4 } from 'uuid';

import './CreateCourse.css';

import { Input } from '../../common/Input/Input';
import { Button } from '../../common/Button/Button';

import {
	mockedAuthorsList,
	nameSection,
	addAuthor,
	authorsList,
	courseAuthors,
	courseDuration,
} from '../../constants';
import { pipeDuration } from '../../helpers/pipeDuration';
import { dateGenerator } from '../../helpers/dateGenerator';

export const CreateCourse = (props) => {
	const [form, setForm] = useState({
		title: '',
		description: '',
		authorName: '',
		duration: '0',
	});
	const [isEmptyAuthorsList, setIsEmptyAuthorsList] = useState(true);
	const [authorsListData, setAuthorsListData] = useState(mockedAuthorsList);
	const [courseAuthorsData, setCourseAuthorsData] = useState([]);

	const time = useMemo(() => pipeDuration(form.duration), [form.duration]);
	const onInputChange = useCallback((event) => {
		const { name, value } = event.target;
		console.log(name, value);

		setForm((prev) => ({
			...prev,
			[name]: value,
		}));
	}, []);

	//Submit Form
	const onSubmitFunction = (e) => {
		e.preventDefault();
		console.log(form);

		if (form.description.length < 2) {
			alert('Please enter more than 2 characters in field "Description"');
			return;
		}

		if (form.duration === '0') {
			alert('Duration should be more than 0');
			return;
		}
		if (courseAuthorsData.length === 0) {
			alert('Please add course author');
			return;
		}
		createCourse();
		props.onClickCreateCourseBtn();
	};

	//Add new Author
	const createAuthor = () => {
		if (form.authorName.length < 2) {
			alert('Please enter more than 2 characters in field "Author name"');
			return;
		}
		const newAuthorId = uuidv4();
		console.log(newAuthorId);

		const newAuthor = {
			id: newAuthorId,
			name: form.authorName,
		};
		setAuthorsListData((prev) => [...prev, newAuthor]);

		setForm((prev) => ({
			...prev,
			authorName: '',
		}));
		console.log(authorsListData);
	};

	//Handle add author to course author list
	const addAuthorToCourseAuthor = (event) => {
		const target = event.target;
		const num = target.parentElement.dataset.number;
		const endNum = +num + 1;
		const newCourseAuthor = authorsListData.slice(num, endNum);

		setAuthorsListData((prev) => [
			...prev.slice(0, num),
			...prev.slice(+num + 1),
		]);

		setCourseAuthorsData((prev) =>
			prev.length === 0 ? newCourseAuthor : [...prev, ...newCourseAuthor]
		);
		setIsEmptyAuthorsList((prev) => false);
	};
	//Handle delete author from course author list, add to authors list
	const deleteAuthorFromCourseAuthor = (event) => {
		const target = event.target;
		const num = target.parentElement.dataset.number;
		const endNum = +num + 1;
		const newCourseAuthor = courseAuthorsData.slice(num, endNum);

		setCourseAuthorsData((prev) => [
			...prev.slice(0, num),
			...prev.slice(+num + 1),
		]);

		setAuthorsListData((prev) => [...prev, ...newCourseAuthor]);
		setIsEmptyAuthorsList((prev) =>
			courseAuthorsData.length === 1 ? true : false
		);
	};

	//Create new Course and add to Courses List
	const createCourse = () => {
		const newCourseId = uuidv4();
		const date = dateGenerator();
		const authorsArr = courseAuthorsData.map((author) => author.name);
		console.log(courseAuthorsData);

		const newCourse = {
			id: newCourseId,
			title: form.title,
			description: form.description,
			creationDate: date,
			duration: form.duration,
			authors: authorsArr,
		};
		console.log(newCourse);
		props.addNewCourseToList(newCourse);
	};

	//Lists
	const authorsListItem = authorsListData.map((author, i) => (
		<li className='authors-list__item' key={author.id} data-number={i}>
			<p className='author-name'>{author.name}</p>
			<Button {...authorsList.button} onClick={addAuthorToCourseAuthor} />
		</li>
	));

	const courseAuthorsListItem = courseAuthorsData.map((author, i) => (
		<li className='course-authors__list-item' key={author.id} data-number={i}>
			<p className='author-name'>{author.name}</p>
			<Button
				{...courseAuthors.button}
				onClick={deleteAuthorFromCourseAuthor}
			/>
		</li>
	));

	return (
		<form className='create-course__form' onSubmit={onSubmitFunction}>
			<fieldset className='course-name'>
				<div className='course-header'>
					<Input
						{...nameSection.input}
						value={form.title}
						onChange={onInputChange}
					/>
					<Button {...nameSection.button} />
				</div>
				<label className={nameSection.labelTextarea.class}>
					{nameSection.labelTextarea.name}
					<textarea
						{...nameSection.description}
						value={form.description}
						onChange={onInputChange}
					></textarea>
				</label>
			</fieldset>
			<fieldset className='course-authors'>
				<div className='authors__wrap'>
					<div className='add-author__container'>
						<h3 className='title'>{addAuthor.title}</h3>
						<Input
							{...addAuthor.input}
							value={form.authorName}
							onChange={onInputChange}
						/>
						<Button {...addAuthor.button} onClick={createAuthor} />
					</div>
					<div className='authors-list__container'>
						<h3 className='title'>{authorsList.title}</h3>
						<ul className='authors-list'>{authorsListItem}</ul>
					</div>
				</div>
				<div className='course-details'>
					<div className='course-duration'>
						<h3 className='title'>{courseDuration.title}</h3>
						<Input
							{...courseDuration.input}
							value={form.duration === '0' ? '' : form.duration}
							onChange={onInputChange}
						/>
						<p className='course-duration__time'>Time: {time}</p>
					</div>
					<div className='course-authors'>
						<h3 className='title'>{courseAuthors.title}</h3>
						{isEmptyAuthorsList ? (
							<p className='create-course__text'>Authors list is empty</p>
						) : (
							<ul className='course-authors__list'>{courseAuthorsListItem}</ul>
						)}
					</div>
				</div>
			</fieldset>
		</form>
	);
};
