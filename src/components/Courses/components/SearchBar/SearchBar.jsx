import './SearchBar.css';

import { Input } from '../../../../common/Input/Input';
import { Button } from '../../../../common/Button/Button';

export const SearchBar = (props) => {
	return (
		<div className='search-bar'>
			<Input
				label={props.label.labelClass}
				text={props.label.text}
				name='search'
				value={props.value}
				input={props.input.inputClass}
				placeholder={props.input.placeholder}
				onChange={props.onChange}
			/>
			<Button
				classBtn={props.button.name}
				buttonTexte={props.button.buttonTexte}
				onClick={props.onClick}
			/>
		</div>
	);
};
