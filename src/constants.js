export const mockedCoursesList = [
	{
		id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c551ba',
		title: 'JavaScript',
		description: `Lorem Ipsum is simply dummy text of the printing and
      typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived
      not only five centuries, but also the leap into electronictypesetting,
      remaining essentially unchanged.`,
		creationDate: '8/3/2021',
		duration: 160,
		authors: [
			'27cc3006-e93a-4748-8ca8-73d06aa93b6d',
			'f762978b-61eb-4096-812b-ebde22838167',
		],
	},
	{
		id: 'b5630fdd-7bf7-4d39-b75a-2b5906fd0916',
		title: 'Angular',
		description: `Lorem Ipsum is simply dummy text of the printing and
      typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`,
		creationDate: '10/11/2020',
		duration: 210,
		authors: [
			'df32994e-b23d-497c-9e4d-84e4dc02882f',
			'095a1817-d45b-4ed7-9cf7-b2417bcbf748',
		],
	},
];

export const mockedAuthorsList = [
	{
		id: '27cc3006-e93a-4748-8ca8-73d06aa93b6d',
		name: 'Vasiliy Dobkin',
	},
	{
		id: 'f762978b-61eb-4096-812b-ebde22838167',
		name: 'Nicolas Kim',
	},
	{
		id: 'df32994e-b23d-497c-9e4d-84e4dc02882f',
		name: 'Anna Sidorenko',
	},
	{
		id: '095a1817-d45b-4ed7-9cf7-b2417bcbf748',
		name: 'Valentina Larina',
	},
];

//CreateCourse Component

export const nameSection = {
	input: {
		label: 'create-course__label label__minimized',
		text: 'Title',
		name: 'title',
		input: 'create-course__input',
		placeholder: 'Enter title...',
		required: true,
	},
	button: {
		classBtn: 'styledBtn create-course__btn',
		buttonTexte: 'Create course',
		type: 'submit',
	},
	labelTextarea: {
		class: 'create-course__label',
		name: 'Description',
	},
	description: {
		className: 'course-description',
		name: 'description',
		cols: '30',
		rows: '5',
		placeholder: 'Enter description',
		required: true,
		minLength: 2,
	},
};

export const addAuthor = {
	input: {
		label: 'create-course__label',
		text: 'Author name',
		name: 'authorName',
		input: 'create-course__input',
		placeholder: 'Enter author name...',
		required: false,
		type: 'text',
	},
	button: {
		classBtn: 'styledBtn create-course__btn',
		buttonTexte: 'Create author',
		type: 'button',
	},
	title: 'Add author',
};

export const authorsList = {
	button: {
		classBtn: 'styledBtn create-course__btn',
		buttonTexte: 'Add author',
		type: 'button',
	},
	title: 'Authors',
};

export const courseAuthors = {
	button: {
		classBtn: 'styledBtn create-course__btn',
		buttonTexte: 'Delete author',
		type: 'button',
	},
	title: 'Course authors',
};

export const courseDuration = {
	input: {
		label: 'create-course__label',
		text: 'Duration',
		name: 'duration',
		input: 'create-course__input',
		placeholder: 'Enter duration in minutes...',
		required: true,
		type: 'number',
	},
	title: 'Duration',
};
