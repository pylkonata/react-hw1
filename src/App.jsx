import { useState } from 'react';

import './App.css';
import image from './assets/img/logo.png';

import { Header } from './components/Header/Header';
import { Courses } from './components/Courses/Courses';
import { CreateCourse } from './components/CreateCourse/CreateCourse';

import courseListMaker from './helpers/courseListMaker';

const onClick = () => {
	console.log('Logout');
};

const header = {
	button: {
		buttonTexte: 'Logout',
		name: 'styledBtn',
		onClick: onClick,
	},
	user: {
		name: 'Nata',
	},
};

const App = () => {
	const coursesList = courseListMaker();

	const [isShownCourse, setIsShownCourse] = useState(true);
	const [listCourses, setListCourses] = useState(coursesList);

	const handleShownCourse = () => {
		setIsShownCourse((prevIsShownCourse) => !prevIsShownCourse);
	};
	console.log(isShownCourse);
	const changeCoursesList = (newCourse) => {
		setListCourses((prev) => [...prev, newCourse]);
	};
	return (
		<div className='app'>
			<Header src={image} button={header.button} user={header.user} />
			{isShownCourse ? (
				<Courses
					onClickAddCourseBtn={handleShownCourse}
					listCoursesData={listCourses}
				/>
			) : (
				<CreateCourse
					onClickCreateCourseBtn={handleShownCourse}
					addNewCourseToList={changeCoursesList}
				/>
			)}
		</div>
	);
};

export default App;
