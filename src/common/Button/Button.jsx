import './Button.css';

export const Button = (props) => {
	return (
		<button
			className={props.classBtn}
			onClick={props.onClick}
			type={props.type}
		>
			{props.buttonTexte}
		</button>
	);
};
