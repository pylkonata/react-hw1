import { mockedCoursesList, mockedAuthorsList } from '../constants';

const courseListMaker = () => {
	const courseList = JSON.parse(JSON.stringify(mockedCoursesList));
	const authorList = mockedAuthorsList;

	courseList.forEach((course) => {
		course.authors.forEach((author, id, arr) => {
			arr[id] = authorList.find((item) => item.id === author).name;
		});
		course.authors = course.authors.join(', ');
	});
	console.log(courseList);

	return courseList;
};
export default courseListMaker;
