import './CourseCard.css';
import { Button } from '../../../../common/Button/Button';
import { dateGenerator } from '../../../../helpers/dateGenerator';
import { pipeDuration } from '../../../../helpers/pipeDuration';

export const CourseCard = (props) => {
	const onClick = () => {
		console.log('go to course');
	};

	const button = {
		buttonTexte: 'Show course',
		name: 'styledBtn card-btn',
		onClick: onClick,
	};

	return (
		<li className='course-card'>
			<div className='course-card__container'>
				<h3 className='course-card__title'>{props.card.title}</h3>
				<p className='course-card__text'>{props.card.description}</p>
			</div>
			<div className='course-card__container'>
				<p className='course-card__text'>
					<span className='text__bold'>Authors: </span>
					{props.card.authors}
				</p>
				<p className='course-card__text'>
					<span className='text__bold'>Duration: </span>
					{pipeDuration(props.card.duration)}
				</p>
				<p className='course-card__text'>
					<span className='text__bold'>Created: </span>
					{dateGenerator(props.card.creationDate)}
				</p>
				<Button
					classBtn={button.name}
					buttonTexte={button.buttonTexte}
					onClick={button.onClick}
				/>
			</div>
		</li>
	);
};
