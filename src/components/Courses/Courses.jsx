import { useState } from 'react';

import './Courses.css';

import { CourseCard } from './components/CourseCard/CourseCard';
import { SearchBar } from './components/SearchBar/SearchBar';
import { Button } from '../../common/Button/Button';

const searchBar = {
	label: {
		labelClass: 'search-bar__label',
		text: '',
	},
	input: {
		inputClass: 'search-bar__input',
		placeholder: 'Enter course name...',
	},
	button: {
		name: 'styledBtn',
		buttonTexte: 'Search',
	},
};

const button = {
	name: 'styledBtn',
	buttonTexte: 'Add new course',
};

export const Courses = (props) => {
	const listCourses = [...props.listCoursesData];

	const [list, setList] = useState(listCourses);
	const [search, setSearch] = useState('');

	const onClickAddBtn = () => props.onClickAddCourseBtn();

	const onChangeInput = (e) => {
		setSearch(e.target.value);
		console.log(e.target.value);

		if (!e.target.value) setList(listCourses);
	};

	const onClickSearchBtn = () => {
		const listCoursesSearched = list.filter(
			(course) =>
				course.title.toLowerCase().includes(search.toLowerCase()) ||
				course.id.toLowerCase().includes(search.toLowerCase())
		);
		setList((prev) => [...listCoursesSearched]);

		console.log(listCoursesSearched);
	};

	let listItems = list.map((course) => (
		<CourseCard key={course.id} card={course} />
	));

	return (
		<div className='courses-wrap'>
			<div className='courses-header'>
				<SearchBar
					label={searchBar.label}
					value={search}
					onChange={onChangeInput}
					input={searchBar.input}
					button={searchBar.button}
					onClick={onClickSearchBtn}
				/>
				<Button
					classBtn={button.name}
					buttonTexte={button.buttonTexte}
					onClick={onClickAddBtn}
				/>
			</div>
			<ul className='courses-list'>{listItems}</ul>
		</div>
	);
};
