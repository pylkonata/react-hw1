export const dateGenerator = (dateString?) => {
	if (dateString) {
		return dateString.replaceAll('/', '.');
	}
	// Date format: d/m/yyyy
	const date = new Date().toLocaleDateString('uk-Uk');

	let [day, month, year] = date.split('.');
	console.log(day, month, year);
	day = day > 10 ? day : day[1];
	month = month > 10 ? month : month[1];
	console.log(day, month, year);

	return [day, month, year].join('.');
};
