export const pipeDuration = (timeMin) => {
	console.log(timeMin);
	if (!timeMin || timeMin <= 0) return `00:00 hours`;
	let minutes = timeMin % 60;
	let hours = (timeMin - minutes) / 60;

	hours = hours < 10 ? `0` + hours : hours;
	minutes = minutes < 10 ? '0' + minutes : minutes;
	return `${hours}:${minutes} hours`;
};
