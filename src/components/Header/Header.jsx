import './Header.css';
import { Logo } from './components/Logo/Logo';
import { Button } from '../../common/Button/Button';

export const Header = (props) => {
	return (
		<header className='header'>
			<Logo src={props.src} />
			<div className='user-container'>
				<p className='user-name'>{props.user.name}</p>
				<Button
					classBtn={props.button.name}
					buttonTexte={props.button.buttonTexte}
					onClick={props.button.onClick}
				/>
			</div>
		</header>
	);
};
