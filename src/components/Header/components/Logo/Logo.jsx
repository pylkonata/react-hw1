import './Logo.css';

export const Logo = (props) => {
	return <img className='logo' src={props.src} alt='logo' />;
};
