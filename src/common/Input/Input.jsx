import './Input.css';

export const Input = (props) => {
	return (
		<label className={props.label}>
			{props.text}
			<input
				name={props.name}
				value={props.value}
				type={props.type}
				className={props.input}
				placeholder={props.placeholder}
				onChange={props.onChange}
				required={props.required}
			/>
		</label>
	);
};
